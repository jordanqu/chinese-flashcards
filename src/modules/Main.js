import React, { Component } from 'react';


import { Home } from '../pages/Home';

export class Main extends Component { 
    render() {
        const { data } = this.props;
        return (
            <main>
                <Home data={data}/>
            </main>
        );
    }
}
