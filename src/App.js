import React, { Component } from "react";

// ============= MODULES ==================
import { Main } from "./modules/Main";

import data from './data.json';
 

// ============= CSS ==================
import "bootstrap/dist/css/bootstrap.min.css";
import "./sass/App.scss";

class App extends Component {
  state = {
    data: data
  }
  render() {
    const { data } = this.state;
    return (
      <React.Fragment>
        <Main data={data} />
      </React.Fragment>
    );
  }
}

export default App;
