import React, { Component } from "react";

import _ from "lodash";

import Slider from "react-slick";
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

export class Home extends Component {

  state = {
    data: this.props.data,
    activeSlide: 1,
    pinyinOpacity: 100,
    charOpacity: 100,
    englishOpacity: 100,
    prevpinyinOpacity: 100,
    prevcharOpacity: 100,
    prevenglishOpacity: 100,
    toggleMenu: false
  }

  changeCharOpacity = (e) => {
    let userInput = e.target.value.trim();
    this.setState({
      charOpacity: userInput,
    })
  }

  changePinyinOpacity = (e) => {
    let userInput = e.target.value.trim();
    this.setState({
      pinyinOpacity: userInput,
    })
  }

  changeEnglishOpacity = (e) => {
    let userInput = e.target.value.trim();
    this.setState({
      englishOpacity: userInput,
    })
  }

  componentDidMount() {
    const shuffledState = _.shuffle(this.props.data);
    this.setState({
      data: shuffledState
    })
  }

  next = () => {
    this.slider.slickNext();
  }

  prev = () => {
    this.slider.slickPrev();
  }

  peakStart = () => {
    this.setState({
      prevcharOpacity: this.state.charOpacity,
      prevpinyinOpacity: this.state.pinyinOpacity,
      prevenglishOpacity: this.state.englishOpacity,
      charOpacity: 100,
      pinyinOpacity: 100,
      englishOpacity: 100
    })
  }

  peakEnd = () => {
    this.setState({
      charOpacity: this.state.prevcharOpacity,
      pinyinOpacity: this.state.prevpinyinOpacity,
      englishOpacity: this.state.prevenglishOpacity
    })
  }

  handleMenuClick = (e) => {
    this.setState(currentState => {
        return {
            toggleMenu: !currentState.toggleMenu
        };
    });
  }

  render() {
    const settings = {
      dots: false,
      arrows: false,
      infinite: true,
      autoplay: false,
      pauseOnHover: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      afterChange: current => this.setState({ activeSlide: current + 1 })
    }

    const cardCount = _.size(this.state.data);

    return (
      <React.Fragment>

        <div className="menu" style={{display: this.state.toggleMenu === false ? "none" : "block"}}>
          <h2 className="mb-4">Options</h2>
          
          <label>Character Opacity <span>({this.state.charOpacity}%)</span></label>
          <input className="w-100" type="range" id="opacity-character" name="opacity-character" min="0" max="100" onChange={this.changeCharOpacity} defaultValue={this.state.charOpacity}></input>
          
          <label>Pinyin Opacity <span>({this.state.pinyinOpacity}%)</span></label>
          <input className="w-100" type="range" id="opacity-pinyin" name="opacity-pinyin" min="0" max="100" onChange={this.changePinyinOpacity} defaultValue={this.state.pinyinOpacity}></input>
          
          <label>English Opacity <span>({this.state.englishOpacity}%)</span></label>
          <input className="w-100" type="range" id="opacity-english" name="opacity-english" min="0" max="100" onChange={this.changeEnglishOpacity} defaultValue={this.state.englishOpacity}></input>
        </div>

        <div className="header container-fluid py-3">
          <div className="row align-items-center">
            <div className="col-6 text-left">
              <button onClick={this.handleMenuClick}>选项</button>
            </div>
            <div className="col-6 text-right">
              {this.state.activeSlide} / {cardCount}
            </div>
          </div>
        </div>

        <div className="slider-wrap">
          <Slider ref={c => (this.slider = c)} {...settings}>
            {this.state.data.map((card, index) => {
              return (
                <div key={index}>
                  <div>
                    <div className="text-center character">
                      <h1 style={{ opacity: this.state.charOpacity/100 }} className="mb-4">{card.character}</h1>
                    </div>
                    <div className="text-center pinyin">
                      <h3 style={{ opacity: this.state.pinyinOpacity/100 }} className="mb-4">{card.pinyin}</h3>
                    </div>
                    <div className="text-center english px-4">
                      <h4 className="mb-4" style={{ opacity: this.state.englishOpacity/100 }}>{card.english}</h4>
                    </div> 
                    <div className="text-center">
                      <a target="_blank" rel="noopener noreferrer" style={{color:"#fff"}} href={'http://www.strokeorder.info/mandarin.php?q='+card.character}><small><u>View Strokes</u></small></a>
                    </div> 
                  </div>
                </div>
              )
            })}
          </Slider>
        </div>
        <div className="slider-buttons">
          <div className="row">
            <div className="col-12 text-right">
              <svg onMouseDown={this.peakStart} onMouseUp={this.peakEnd} onTouchStart={this.peakStart} onTouchEnd={this.peakEnd} width="60" className="px-3 mr-1 mb-3" version="1.1" x="0px" y="0px" viewBox="0 0 1000 1000" enableBackground="new 0 0 1000 1000"><g><path style={{fill:"#fff"}} d="M500,10C227,10,10,227,10,500s217,490,490,490s490-217,490-490S773,10,500,10z M500,920C269,920,80,731,80,500S269,80,500,80s420,189,420,420S731,920,500,920z"/><path style={{fill:"#fff"}} d="M542,633h-70c0-7,0-14,0-28c0-49,21-91,63-126c49-35,70-70,70-105c0-56-28-77-84-84c-56,0-91,35-112,105l-70-21c21-105,84-154,196-154c91,7,147,56,154,140c0,56-28,105-91,147c-42,28-63,63-56,98C542,619,542,626,542,633z M549,780h-84v-84h84V780z"/></g></svg>
            </div>
            <div className="col-6 px-0">
              <button className="button w-100 py-2" onClick={this.prev}>&#x2190;</button>
            </div>
            <div className="col-6 px-0">
              <button className="button w-100 py-2" onClick={this.next}>&#x2192;</button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
